public class Exercise2Exam {
    public static void main(String[] args) {
        StrThread StrThread1 = new StrThread(); // we create the 2 threads
        StrThread StrThread2 = new StrThread();

        StrThread1.setName("StrThread1"); // we rename the threads
        StrThread2.setName("StrThread2");

        StrThread1.start(); // we start the 2 threads
        StrThread2.start();
    }
}

class StrThread extends Thread{
    @Override
    public void run() {
        for(int i = 0; i < 5; i++){
            long currentTime = System.currentTimeMillis();
            System.out.println(this.getName() + " - " + currentTime);
            try{
                Thread.sleep(5000);
            }
            catch (Exception e){
                System.out.println("Error");;
            }
        }
    }
}