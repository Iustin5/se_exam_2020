// The UML diagram implementation

public class Exercise1Exam {
    public static void main(String[] args) {

    }
}

class A{

}

class B extends A{ // inheritance
    private String param;
    E e;
    Z z;
    B(E e){
        this.e = e; // aggregation
        z.g(); // dependency
    }
}

class Z{
    public void g(){

    }
}

class D{
    B b; // association
    public void f(){

    }
}

class C{
    B b;
    C(){
        b = new B(new E()); // composition
    }
}

class E{

}
